import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

var CONSTANTS = {

    ENDPOINT: 'http://35.237.141.95:49160/microservicioVBC/',
    ENDPOINTS: 'http://35.237.141.95:49160/microservicioVBC/'
} 

/* var CONSTANTS = {

    ENDPOINT: 'http://localhost:3000/microservicioVBC/',
    ENDPOINTS: 'http://localhost:3000/microservicioVBC/'
}  */


@Injectable()
export class MicroservicioService {
    public endpoints: any = {
        microservicioVBC: {
            url: CONSTANTS.ENDPOINT,
            urlS: CONSTANTS.ENDPOINTS,
            path: {
                obtenerServicios: 'obtenerServicios',
                obtenerCategorias: 'obtenerCategorias',
                crearServicios: 'crearServicios',
                crearCategoria: 'crearCategoria',
                eliminarServicio: 'eliminarServicio',
                actualizarServicio: 'actualizarServicio',
                enviarEmail: 'enviarEmail',
                crearUsuario: 'crearUsuario',
                crearCliente: 'crearCliente',
                obtenerEmpleados: 'obtenerEmpleados',
                crearReserva: 'crearReserva',
                obtenerReserva: 'obtenerReserva'
            },
        },
    };
    constructor(private http: HttpClient) { }
    getEndpoint(bff: string) {
        return this.endpoints[bff];
    }
    getEndpointS(bffS: string) {
        return this.endpoints[bffS];
    }
    resolveUrl(bff: string, path?: string) {
        const endpoint = this.getEndpoint(bff);
        if (!endpoint) {
            return bff + (path ? path : '');
        }
        return endpoint.url + (endpoint.url && endpoint.path[path]) || path || '';
    }
    resolveUrlS(bffS: string, path?: string) {
        const endpoint = this.getEndpointS(bffS);
        if (!endpoint) {
            return bffS + (path ? path : '');
        }
        return endpoint.urlS + (endpoint.urlS && endpoint.path[path]) || path || '';
    }
    makeGetRequestSin(bffS: string, path: string, data?: any): Observable<any> {
        let tempHeader = {
            url: this.resolveUrlS(bffS, path),
        };
       
        return this.http.get(this.resolveUrlS(bffS, path), { headers: tempHeader });
    }

    makeGetRequest2Sin(bffS: string, path: string, data: any): Observable<any> {
        let tempHeader = {
            url: this.resolveUrlS(bffS, path),
            fieldname: data.fieldname,
            value: data.value
        };
       
        return this.http.get(this.resolveUrlS(bffS, path), { headers: tempHeader });
    }

    makePostRequest(bffS: string, path: string, data: any): Observable<any> {
        let tempHeader = {
            url: this.resolveUrlS(bffS, path),
            datos: data 
        };
        return this.http.post(this.resolveUrlS(bffS, path), data, { headers: tempHeader });

    }
}