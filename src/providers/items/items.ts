import { Injectable } from '@angular/core';

import { Item } from 'src/models/item';
import { Api } from '../api/api';
import { MicroservicioService } from '../microservicio/ms.service';
import { queryModel } from 'src/models';

@Injectable()
export class Items {

  constructor(public api: Api, private msservice:MicroservicioService,private queryModel:queryModel) {
   
  }

  ionViewDidLoad(){
    
  }

  query(params?: any) {
    return this.api.get('/items', params);
  }

  add(item: Item) {
  }

  delete(item: Item) {
  }

}
