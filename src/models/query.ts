export class queryModel{

    private _listaServicios: Array<Object>;
    private _listaCategorias: Array<Object>;
    private _listaEmpleados: Array<Object>;
    constructor() { }


    get listaServicios() {
        return this._listaServicios;
    }

    // Set
    set listaServicios(param: Array<Object>) {
        this._listaServicios = param;
    }

    get listaCategorias() {
        return this._listaCategorias;
    }

    // Set
    set listaCategorias(param: Array<Object>) {
        this._listaCategorias = param;
    }

    
    get listaEmpleados() {
        return this._listaEmpleados;
    }

    // Set
    set listaEmpleados(param: Array<Object>) {
        this._listaEmpleados = param;
    }
}