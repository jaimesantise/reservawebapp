import { Injectable } from '@angular/core';

import { Item } from 'src/models/item';
import { queryModel } from 'src/models';
import { MicroservicioService } from 'src/providers/microservicio/ms.service';


@Injectable()
export class Items {
  items: Item[] = [];

  defaultItem: any = {
    "name": "Burt Bear",
    "profilePic": "assets/img/speakers/bear.jpg",
    "cost": "5000",
    "about": "Burt is a Bear.",
  };


  constructor(private queryModel: queryModel, private msservice:MicroservicioService) {

    
    /* this.msservice.makeGetRequestSin('microservicioVBC','obtenerServicios')
    .subscribe((response) => {
      this.queryModel.listaServicios = response;
      console.log(response[0]);
    },(error) => {
      console.log("Error: " + error);
    },
    () => {
      
      for (let responseItem of this.queryModel.listaServicios) {
        this.items.push(new Item({
          "name": responseItem["nombre"],
          "profilePic": "assets/img/speakers/bear.jpg",
          "cost": responseItem["valor"],
          "about": responseItem["descripcion"],
          "duracion": responseItem["duracion"]
        })
      );
      }
    }
    ); */
  }

  query(params?: any) {
    if (!params) {
      return this.items;
    }

    return this.items.filter((item) => {
      for (let key in params) {
        let field = item[key];
        if (typeof field == 'string' && field.toLowerCase().indexOf(params[key].toLowerCase()) >= 0) {
          return item;
        } else if (field == params[key]) {
          return item;
        }
      }
      return null;
    });
  }

  add(item: Item) {
    this.items.push(item);
  }

  delete(item: Item) {
    this.items.splice(this.items.indexOf(item), 1);
  }
}
