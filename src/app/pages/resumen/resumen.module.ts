import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ResumenPage } from './resumen';
import { IonicStorageModule } from '@ionic/storage';

@NgModule({
  declarations: [
    ResumenPage,
  ],
  imports: [
    IonicPageModule.forChild(ResumenPage),
    IonicStorageModule.forRoot()
  ],
})
export class ResumenPageModule {}
