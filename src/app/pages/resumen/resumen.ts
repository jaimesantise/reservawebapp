import { Component, ViewChild } from '@angular/core';
import { Storage } from '@ionic/storage';
import { IonicPage, NavController, NavParams, ViewController, Nav, App, DateTime } from 'ionic-angular';
import { MainPage } from '../';
import { MicroservicioService } from 'src/providers/microservicio/ms.service';

/**
 * Generated class for the ResumenPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-resumen',
  templateUrl: 'resumen.html',
})
export class ResumenPage {
  private nombreCliente: string;
  private serviciosReservados: string = '';
  private fechaCita: Date;
  private email: string;
  private nombreEstilista: string;
  private contacto: string = '+56 9 87654321 / +56 2 27654321'
  private citaDiaHora: string;
  private direccion: string = "Av. Libertador Bernardo O'Higgins 1234, Galería X, local 100";

  @ViewChild(Nav) nav: Nav;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public storage: Storage,
    public viewCtrl: ViewController,
    private msservice: MicroservicioService,
    public appCtrl: App) {

    this.storage.get('nombreCliente').then((val) => {
      this.nombreCliente = val;

      this.storage.get('mesSeleccionado').then((month) => {
        this.storage.get('anoSeleccionado').then((year) => {
          this.storage.get('diaSeleccionado').then((day) => {
            this.storage.get('horaSeleccionada').then((hour) => {
              this.citaDiaHora = day.dayOfWeekName + ", " + day.daySelected + " de " + month.monthName + " del " + year + " a las " + hour + " hrs.";


              this.storage.get('serviciosSeleccionados').then((val) => {
                val.forEach(serv => {
                  this.serviciosReservados += serv.name + " + ";
                });

                this.storage.get('estilista').then((val) => {
                  this.nombreEstilista = val["nombre"] + " " + val["apellidos"];//val;

                  this.storage.get('email').then((val) => {
                    this.email = val;

                    //ENVIO DE CORREOS
                    const datosEmail = {
                      destinatario: this.email,
                      asuntoCorreo: "Reserva de cita",
                      nombreCliente: this.nombreCliente,
                      serviciosReservados: this.serviciosReservados,
                      citaDiaHora: this.citaDiaHora,
                      direccion: this.direccion,
                      nombreEstilista: this.nombreEstilista,
                      contacto: this.contacto
                    }

                    this.msservice.makePostRequest('microservicioVBC', 'enviarEmail', datosEmail).subscribe(
                      (response) => {
                        console.log("OK: " + response);
                      },
                      (error) => {
                        console.log("Error: " + error);
                      },
                      () => {

                      });
                  });
                });

              });

            });
          });
        });
      });
    });

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ResumenPage');

  }

  goToHome() {
    this.viewCtrl.dismiss();
    this.appCtrl.getRootNav().push(MainPage);
  }

}
