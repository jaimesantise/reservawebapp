import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { MainPage } from '../';
/**
 * Generated class for the ShoppingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-shopping-cart',
  templateUrl: 'shopping-cart.html',
})
export class ShoppingCartPage {
  serviciosSeleccionados: Array<Object> = [];
  totalServicios: number = 0;
  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private storage: Storage) {
      
  }

  ionViewDidLoad() {
        
  }

  ionViewWillEnter(){
    this.totalServicios = 0;

    this.storage.get('serviciosSeleccionados').then((val) => {
      console.log(val);
      this.serviciosSeleccionados= val;

      this.serviciosSeleccionados.forEach(serv => {
        this.totalServicios += Number(serv['price'].replace('.','').replace(',',''));
      })

    });

  }

  goServices(){
    this.navCtrl.push(MainPage);
  }

  deleteFromCart(service){
    let index = this.serviciosSeleccionados.indexOf(service);

    if(index > -1){
      this.serviciosSeleccionados.splice(index, 1);
      this.storage.set('serviciosSeleccionados', this.serviciosSeleccionados);
      this.totalServicios -= Number(service['price'].replace('.','').replace(',',''));
    }
  }

  goToReserve(){
    this.navCtrl.push('CalendarPage');
  }

}
