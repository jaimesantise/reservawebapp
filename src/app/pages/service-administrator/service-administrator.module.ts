import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ServiceAdministratorPage } from './service-administrator';

@NgModule({
  declarations: [
    ServiceAdministratorPage,
  ],
  imports: [
    IonicPageModule.forChild(ServiceAdministratorPage),
  ],
})
export class ServiceAdministratorPageModule {}
