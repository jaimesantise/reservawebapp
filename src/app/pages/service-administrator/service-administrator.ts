import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';

/**
 * Generated class for the ServiceAdministratorPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-service-administrator',
  templateUrl: 'service-administrator.html',
})
export class ServiceAdministratorPage {

  constructor(public navCtrl: NavController, 
    public navParams: NavParams,
    public modalCtrl: ModalController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ServiceAdministratorPage');
  }

  addItem() {
    let addModal = this.modalCtrl.create('ItemCreatePage');
    addModal.onDidDismiss(item => {
   
    })
    addModal.present();
  
  }

  updateDeleteItem(){
    this.navCtrl.push('ServicesOnButtonsPage', {
      origen: 'EDITAR-ELIMINAR'
    });
  }

  addCategoria() {
    let addModal = this.modalCtrl.create('CategoryCreatePage');
    addModal.onDidDismiss(category => {
   
    })
    addModal.present();
  
  }

}
