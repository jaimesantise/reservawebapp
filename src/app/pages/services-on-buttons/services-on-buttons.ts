import { Component } from '@angular/core';
import { NavController, IonicPage, LoadingController, ModalController, NavParams, AlertController } from 'ionic-angular';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map'
import { Items } from 'src/providers';
import { Item } from 'src/models/item';
import { MicroservicioService } from 'src/providers/microservicio/ms.service';
import { queryModel } from 'src/models';
import { Storage } from '@ionic/storage';


/**
 * Generated class for the ServicesOnButtonsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-services-on-buttons',
  templateUrl: 'services-on-buttons.html',
})
export class ServicesOnButtonsPage {
    information: any[];
    currentItems: Item[];
    respuesta: any;
    origen: string;
    serviciosSeleccionados: Array<Object> = [];
    servicios: Array<Object> = [];
    serviciosFiltrados: Array<Object> = [];
    hayResultados: boolean = true;
    constructor(
      public navCtrl: NavController,
      public navParams: NavParams,
      private alertCtrl: AlertController,
      private http: Http,
      private items: Items,
      private msservice: MicroservicioService,
      private modelo: queryModel,
      private loadingCtrl: LoadingController,
      private storage: Storage,
      public modalCtrl: ModalController) {
  
      storage.set('serviciosSeleccionados', this.serviciosSeleccionados);
  
    }
  
    ionViewDidLoad() {
      //this.item = this.navParams.get('item') || items.defaultItem;
      this.origen = this.navParams.get('origen') || 'VISTA-CLIENTE';
  
      let loading = this.loadingCtrl.create({
        content: 'Cargando...'
      });
  
      loading.present();
  
      this.modelo.listaServicios = [];
      this.msservice.makeGetRequestSin('microservicioVBC', 'obtenerCategorias').subscribe(
        (response) => {
          console.log(response);
          this.modelo.listaCategorias = response;
  
          this.msservice.makeGetRequestSin('microservicioVBC', 'obtenerServicios').subscribe(
            (response) => {
              console.log(response);
              this.respuesta = response;
            },
            (error) => {
              loading.dismiss();
              console.log("Error: " + error);
              this.navCtrl.push('ErrorServiciosPage');
            },
            () => {
  
              for (let categoria of this.modelo.listaCategorias) {
                let categorias = {
                  name: categoria['nombre'],
                  children: []
                };
  
                for (let servicio of this.respuesta) {
                  if (servicio.categoria == categoria['nombre']) {
                    let servicioX = {
                      "name": servicio.nombre,
                      "information": servicio.descripcion,
                      "price": servicio.valor.toLocaleString('de-DE'),
                      "duration": servicio.duracion,
                      "profilePic": "assets/imgs/pre-depilacion.jpg", //servicio.profilePic
                      "category": servicio.categoria,
                      "servicioid": servicio.servicioid
                    }
                    categorias.children.push(servicioX);
                    this.servicios.push(servicioX);
                  }
                }
  
                this.modelo.listaServicios.push(categorias);
              }
  
              this.information = this.modelo.listaServicios;
              console.log(this.information);
            }
          );
        },
        (error) => {
          loading.dismiss();
          console.log("Error: " + error);
          this.navCtrl.push('ErrorServiciosPage');
        },
        () => {
          loading.dismiss();
        });
    }
  
    ionViewWillEnter() {
      this.storage.get('serviciosSeleccionados').then((val) => {
        console.log(val);
        this.serviciosSeleccionados = val;
  
      });
  
    }
  
  
    toggleSection(i) {
      this.information[i].open = !this.information[i].open;
    }
  
    toggleItem(i, j) {
      this.information[i].children[j].open = !this.information[i].children[j].open;
    }
  
    deleteItem(item) {
      this.items.delete(item);
    }
  
    /**
     * Navigate to the detail page for this item.
     */
    openItem(item: Item) {
      
      if (this.origen == 'VISTA-CLIENTE') {
        this.navCtrl.push('ItemDetailPage', {
          item: item
        });
      }
      else {
        this.navCtrl.push('ItemEditPage', {
          item: item
        });
      }
  
    }
  
    filterItems(ev: any) {
      let val = ev.target.value;
  
      if (val && val.trim() !== '') {
        this.serviciosFiltrados = this.servicios.filter(function (info) {
          const serv = info;
          return info["name"].toLowerCase().includes(val.toLowerCase());
        });
  
        if(this.serviciosFiltrados.length == 0 ){
          this.hayResultados = false;
        }
        else{
          this.hayResultados = true;
        }
      }
    }
  
    limpiaFiltro(ev: any) {
      let val = ev._value;
      if (val == undefined || val.trim() === '') {
        this.serviciosFiltrados = [];
        this.hayResultados = true;
      }
    }
  
    addCart(item: Item) {
      //agregar a localstorage
      this.serviciosSeleccionados.push(item);
      this.storage.set('serviciosSeleccionados', this.serviciosSeleccionados);
  
      // Or to get a key/value pair
      this.storage.get('serviciosSeleccionados').then((val) => {
        console.log('Your age is', val);
      });
      this.presentAlert();
    }
  
    presentAlert() {
      let alert = this.alertCtrl.create({
        title: '',
        subTitle: '¡Servicio agregado correctamente!',
        buttons: ['Aceptar']
      });
      alert.present();
    }
  
  }