import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ServicesOnButtonsPage } from './services-on-buttons';
import { HttpModule } from '@angular/http';

@NgModule({
  declarations: [
    ServicesOnButtonsPage,
  ],
  imports: [
    IonicPageModule.forChild(ServicesOnButtonsPage),
    HttpModule
  ],
  exports: [
    ServicesOnButtonsPage
  ]
})
export class ServicesOnButtonsPageModule {}
