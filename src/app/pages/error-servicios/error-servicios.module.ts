import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ErrorServiciosPage } from './error-servicios';

@NgModule({
  declarations: [
    ErrorServiciosPage,
  ],
  imports: [
    IonicPageModule.forChild(ErrorServiciosPage),
  ],
})
export class ErrorServiciosPageModule {}
