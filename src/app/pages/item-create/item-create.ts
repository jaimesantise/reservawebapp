import { Component, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { IonicPage, NavController, ViewController, LoadingController, AlertController } from 'ionic-angular';
import { MicroservicioService } from 'src/providers/microservicio/ms.service';
import { queryModel } from 'src/models';

@IonicPage()
@Component({
  selector: 'page-item-create',
  templateUrl: 'item-create.html'
})
export class ItemCreatePage {
  @ViewChild('fileInput') fileInput;

  isReadyToSave: boolean;

  item: any;

  form: FormGroup;

  private listaCategorias = [];

  private defaultValue = {
    value: 'Seleccione'
  };

  constructor(public navCtrl: NavController,
    public viewCtrl: ViewController,
    formBuilder: FormBuilder,
    private loadingCtrl: LoadingController,
    private alertCtrl: AlertController,
    private modelo: queryModel,
    private msservice: MicroservicioService) {
    this.form = formBuilder.group({
      profilePic: [''],
      name: ['', Validators.required],
      about: [''],
      cost: [''],
      duracion: [''],
      category: ['']
    });

    // Watch the form for changes, and
    this.form.valueChanges.subscribe((v) => {
      this.isReadyToSave = this.form.valid;
    });
  }

  ionViewDidLoad() {
    
    let loading = this.loadingCtrl.create({
      content: 'Cargando...'
    });

    loading.present();

    this.msservice.makeGetRequestSin('microservicioVBC', 'obtenerCategorias').subscribe(
      (response) => {
        this.modelo.listaCategorias = response;
      },
      (error) => {
        loading.dismiss();
        console.log("Error: " + error);
        this.navCtrl.push('ErrorServiciosPage');
      },
      () => {
        loading.dismiss();
        this.modelo.listaCategorias.forEach(cat => {
          this.listaCategorias.push(cat);
        })
       
      });
  }


  getPicture() {
   /*  if (Camera['installed']()) {
      this.camera.getPicture({
        destinationType: this.camera.DestinationType.DATA_URL,
        targetWidth: 96,
        targetHeight: 96
      }).then((data) => {
        this.form.patchValue({ 'profilePic': 'data:image/jpg;base64,' + data });
      }, (err) => {
        alert('Unable to take photo');
      })
    } else {
      this.fileInput.nativeElement.click();
    } */
  }

  processWebImage(event) {
    let reader = new FileReader();
    reader.onload = (readerEvent) => {

      let imageData = (readerEvent.target as any).result;
      this.form.patchValue({ 'profilePic': imageData });
    };

    reader.readAsDataURL(event.target.files[0]);
  }

  getProfileImageStyle() {
    return 'url(' + this.form.controls['profilePic'].value + ')'
  }

  /**
   * The user cancelled, so we dismiss without sending data back.
   */
  cancel() {
    this.viewCtrl.dismiss();
  }

  /**
   * The user is done and wants to create the item, so return it
   * back to the presenter.
   */
  done() {
    
    if (!this.form.valid) {

    }
    else {
      let loading = this.loadingCtrl.create({
        content: 'Por favor espere...'
      });

      loading.present();

      const new_item = this.form.value;
      /*const data = []
      const newservice = [];


      newservice.push(new_item.name);
      newservice.push(new_item.about);
      newservice.push(new_item.duracion);
      newservice.push(new_item.cost);
      newservice.push(new_item.category["nombre"]);

      data.push(newservice); */

      const nuevoServicio = {
        "nombre": new_item.name,
        "descripcion": new_item.about,
        "duracion": new_item.duracion,
        "valor": new_item.cost,
        "categoria": new_item.category
      }
      this.msservice.makePostRequest('microservicioVBC', 'crearServicios', nuevoServicio)
        .subscribe((response) => {
          loading.dismiss();
          this.presentAlert();
          this.viewCtrl.dismiss(this.form.value);
        }, (error) => {
          loading.dismiss();
          console.log("Error: " + error);
          this.navCtrl.push('ErrorServiciosPage');
        });
    }

  }

  presentAlert() {
    let alert = this.alertCtrl.create({
      title: 'Resultado',
      subTitle: '¡El servicio se creó correctamente en la base de datos!',
      buttons: ['Aceptar']
    });
    alert.present();
  }
}
