import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';

import { ClientePage } from './cliente';

@NgModule({
  declarations: [
    ClientePage,
  ],
  imports: [
    IonicPageModule.forChild(ClientePage),
    TranslateModule.forChild()
  ],
  exports: [
    ClientePage
  ]
})
export class SignupPageModule { }
