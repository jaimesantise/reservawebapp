import { Component, NgZone } from '@angular/core';
import { IonicPage, NavController, ToastController, NavParams, LoadingController, AlertController, ModalController } from 'ionic-angular';

import { User } from 'src/providers';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { MicroservicioService } from 'src/providers/microservicio/ms.service';

@IonicPage()
@Component({
    selector: 'page-cliente',
    templateUrl: 'cliente.html'
})
export class ClientePage {
    // The account fields for the login form.
    // If you're using the username field with or without email, make
    // sure to add it to the type
    isReadyToSave: boolean;
    form: FormGroup;
    formatoEmail: boolean = true;
    public emailInput: FormControl;

    account: { nombre: string, apellidos: string, telefono: number, emailInput: string } = {
        nombre: '',
        apellidos: '',
        telefono: null,
        emailInput: ''
    };


    // Our translated text strings
    private signupErrorString: string;

    constructor(public navCtrl: NavController,
        public params: NavParams,
        public user: User,
        formBuilder: FormBuilder,
        public toastCtrl: ToastController,
        private loadingCtrl: LoadingController,
        public alertCtrl: AlertController,
        public modalCtrl: ModalController,
        private zone: NgZone,
        private msservice: MicroservicioService,
        private storage: Storage) {
        this.form = formBuilder.group({
            nombre: ['', Validators.required],
            apellidos: ['', Validators.required],
            telefono: ['', Validators.required],
            emailInput: ['', Validators.email]
        });



        this.form.valueChanges.subscribe((v) => {
            this.isReadyToSave = this.form.valid;
        });
    }

    ionViewWillEnter() {
    }


    doCrearCliente() {
        // Attempt to login in through our User service

        let loading = this.loadingCtrl.create({
            content: 'Cargando...'
        });

        loading.present();

        const cliente = this.form.value;

        this.storage.set('nombreCliente', cliente.nombre);
        this.storage.set('email', cliente.emailInput);

        const nuevoCliente = {
            "nombre": cliente.nombre,
            "apellidos": cliente.apellidos,
            "telefono": cliente.telefono,
            "email": cliente.emailInput
        };

        const nuevaCita = {
            "fecha": "",
            "hora": "",
            "empleadoid": "",
            "clienteid": "",
            "listaservicios": ""
        };

        this.storage.get('formatoFechaCita').then((value) => {
            nuevaCita.fecha = value;
        });

        this.storage.get('horaSeleccionada').then((value) => {
            nuevaCita.hora = value;
        });

        this.storage.get('estilista').then((value) => {
            nuevaCita.empleadoid = value["idempleado"];
        });

        this.storage.get('serviciosSeleccionados').then((value) => {
            nuevaCita.listaservicios = value["serviciosSeleccionados"];
        });

        this.msservice.makePostRequest('microservicioVBC', 'crearCliente', nuevoCliente).subscribe(
            (response) => {
                debugger;
                nuevaCita.clienteid = response["insertId"];
                
                this.msservice.makePostRequest('microservicioVBC', 'crearReserva', nuevaCita).subscribe(
                    (response) => {
                        loading.dismiss();

                        let contactModal = this.modalCtrl.create("ResumenPage");
                        contactModal.present();
                    },
                    (error) => {
                        loading.dismiss();
                        console.log("Error: " + error);
                        this.navCtrl.push('ErrorServiciosPage');
                    },
                    () => {

                    });

            },
            (error) => {
                loading.dismiss();
                console.log("Error: " + error);
                this.navCtrl.push('ErrorServiciosPage');
            },
            () => {

            });

    }

    validateEmailFormat(input) {
        this.zone.run(() => {
            let email = input.srcElement.value;
            if (this.isValid(email)) {
                this.formatoEmail = true;
            }
            else {
                this.formatoEmail = false;
            }
        });
    }

    isValid(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }
}
