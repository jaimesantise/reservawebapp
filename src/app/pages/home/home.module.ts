import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';

import { PopmenuComponent } from './../../components/popmenu/popmenu.component';

import { HomePage } from './home';
import { IonicPageModule } from 'ionic-angular';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    IonicPageModule.forChild(HomePage),
  ],
  declarations: [HomePage, PopmenuComponent]
  , schemas:[CUSTOM_ELEMENTS_SCHEMA]
})
export class HomePageModule {}
