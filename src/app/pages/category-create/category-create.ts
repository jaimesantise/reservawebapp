import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController, ViewController } from 'ionic-angular';
import { queryModel } from 'src/models';
import { MicroservicioService } from 'src/providers/microservicio/ms.service';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';

/**
 * Generated class for the CategoryCreatePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-category-create',
  templateUrl: 'category-create.html',
})
export class CategoryCreatePage {
  private nombreCategoria: string;
  form: FormGroup;
  isReadyToSave: boolean;

  constructor(public navCtrl: NavController,
    formBuilder: FormBuilder,
    public navParams: NavParams,
    private loadingCtrl: LoadingController,
    private alertCtrl: AlertController,
    public viewCtrl: ViewController,
    private msservice: MicroservicioService) {
    this.form = formBuilder.group({
      category: ['']
    });

    // Watch the form for changes, and
    this.form.valueChanges.subscribe((v) => {
      this.isReadyToSave = this.form.valid;
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CategoryCreatePage');
  }

  done() {
    let loading = this.loadingCtrl.create({
      content: 'Por favor espere...'
    });

    loading.present();

    const data = []
    const newservice = [];


    data.push(newservice);

    const body = {
      "nombre": this.form.value["category"]
    };

    this.msservice.makePostRequest('microservicioVBC', 'crearCategoria', body)
      .subscribe((response) => {
        loading.dismiss();
        this.presentAlert();
        this.viewCtrl.dismiss();
      }, (error) => {
        loading.dismiss();
        console.log("Error: " + error);
        this.navCtrl.push('ErrorServiciosPage');
      });
  }

  presentAlert() {
    let alert = this.alertCtrl.create({
      title: 'Resultado',
      subTitle: '¡La categoría se creó correctamente en la base de datos!',
      buttons: ['Aceptar']
    });
    alert.present();
  }

  cancel() {
    this.viewCtrl.dismiss();
  }
}
