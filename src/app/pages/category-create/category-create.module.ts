import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CategoryCreatePage } from './category-create';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    CategoryCreatePage,
  ],
  imports: [
    IonicPageModule.forChild(CategoryCreatePage),
    TranslateModule.forChild()
  ],
  exports:[
    CategoryCreatePage
  ]
})
export class CategoryCreatePageModule {}
