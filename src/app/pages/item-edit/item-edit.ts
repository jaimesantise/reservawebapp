
import { Component, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { IonicPage, NavController, ViewController, LoadingController, AlertController, NavParams } from 'ionic-angular';
import { MicroservicioService } from 'src/providers/microservicio/ms.service';
import { queryModel } from 'src/models';

/**
 * Generated class for the ItemEditPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-item-edit',
  templateUrl: 'item-edit.html',
})
export class ItemEditPage {

  @ViewChild('fileInput') fileInput;

  isReadyToSave: boolean = true;
  item: any;
  form: FormGroup;
  private listaCategorias = [];

  private defaultValue = {
    value: 'Seleccione'
  };

  constructor(public navCtrl: NavController,
    public viewCtrl: ViewController,
    navParams: NavParams, 
    formBuilder: FormBuilder,
    private loadingCtrl: LoadingController,
    private alertCtrl: AlertController,
    private modelo: queryModel,
    private msservice: MicroservicioService) {
   
      this.form = formBuilder.group({
      name: ['', Validators.required],
      about: ['', Validators.required],
      cost: ['', Validators.required],
      duracion: ['', Validators.required],
      category: ['', Validators.required]
    });

   

    this.item = navParams.get('item') || null;

    if(this.item != null){
      this.form.setValue({
        name: this.item.name,
        about: this.item.information,
        cost: this.item.price.replace('.',''),
        duracion: this.item.duration,
        category: this.item.category
      })
    }

     
  }

  ionViewDidLoad() {
    debugger;
    let loading = this.loadingCtrl.create({
      content: 'Cargando...'
    });

    loading.present();

    this.modelo.listaCategorias.forEach(cat => {
      this.listaCategorias.push(cat);
    })

    loading.dismiss();
  }

  /**
   * The user cancelled, so we dismiss without sending data back.
   */
  cancel() {
    this.viewCtrl.dismiss();
  }

  /**
   * The user is done and wants to create the item, so return it
   * back to the presenter.
   */
  done() {
    debugger;
   
      let loading = this.loadingCtrl.create({
        content: 'Por favor espere...'
      });

      loading.present();
      let nombreCategoria = '';

      const new_item = this.form.value;

      if(new_item.category == undefined){
        nombreCategoria = this.item.category;
      }
      else{
        nombreCategoria = new_item.category;
      }

      const nuevoServicio = {
        "nombre": new_item.name,
        "descripcion": new_item.about,
        "duracion": new_item.duracion,
        "valor": new_item.cost,
        "categoria": nombreCategoria,
        "servicioid": this.item.servicioid
      }
      this.msservice.makePostRequest('microservicioVBC', 'actualizarServicio', nuevoServicio)
        .subscribe((response) => {
          loading.dismiss();
          this.presentAlert('¡El servicio se modificó correctamente en la base de datos!');
          this.viewCtrl.dismiss(this.form.value);
        }, (error) => {
          loading.dismiss();
          console.log("Error: " + error);
          this.navCtrl.push('ErrorServiciosPage');
        });
    

  }

  presentConfirm() {
    let alert = this.alertCtrl.create({
      title: 'Eliminar servicio',
      message: '¿Está seguro que desea eliminar el servicio?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Confirmar',
          handler: () => {
            console.log('Delete clicked');

            let loading = this.loadingCtrl.create({
              content: 'Please wait...'
            });
          
            loading.present();
            let body = {
              "servicioid": this.item.servicioid
            };
            this.msservice.makePostRequest('microservicioVBC', 'eliminarServicio', body).subscribe(
              (response) => {
                loading.dismiss();
                this.presentAlert('La eliminación del servicio finalizó correctamente en la base de datos.');
                this.viewCtrl.dismiss(this.form.value);
              },
              (error) => {
                loading.dismiss();
                console.log("Error: " + error);
                this.navCtrl.push('ErrorServiciosPage');
              },
              () => {}
            );
          }
        }
      ]
    });
    alert.present();
  }
  
  presentAlert(mensaje) {
    let alert = this.alertCtrl.create({
      title: 'Resultado',
      subTitle: mensaje,
      buttons: ['Aceptar']
    });
    alert.present();
  }
}
