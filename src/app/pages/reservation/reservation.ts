import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController, ViewController } from 'ionic-angular';
import { MicroservicioService } from 'src/providers/microservicio/ms.service';
import { dateSortValue } from 'ionic-angular/umd/util/datetime-util';

/**
 * Generated class for the ReservationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-reservation',
  templateUrl: 'reservation.html',
})
export class ReservationPage {

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public msservice: MicroservicioService,
    private loadingCtrl: LoadingController,
    private alertCtrl: AlertController,
    public viewCtrl: ViewController, ) {
  }

  public citas: Array<Object> = [];


  ionViewDidLoad() {
    let loading = this.loadingCtrl.create({
      content: 'Por favor espere...'
    });

    loading.present();

    console.log('ionViewDidLoad ReservationPage');
    const data = {
      fieldname: 'clienteid',
      value: '52'
    };
    this.msservice.makeGetRequest2Sin('microservicioVBC', 'obtenerReserva', data)
      .subscribe((response) => {
              
        for (let cita of response) {
          const dia = new Date(cita.fecha);
          
          this.citas.push({
            hora: cita.hora,
            dia: dia.toLocaleDateString(),
            estilista: cita.nombre_empleado + " " + cita.apellido_empleado,
            servicios: cita.listaservicios.split(','),
          });
         
        }

        loading.dismiss();
       
      }, (error) => {
        loading.dismiss();
        console.log("Error: " + error);
        this.navCtrl.push('ErrorServiciosPage');
      });


    
  }

}
