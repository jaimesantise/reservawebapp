import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';

/**
 * Generated class for the ClientsAdministratorPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-clients-administrator',
  templateUrl: 'clients-administrator.html',
})
export class ClientsAdministratorPage {

  constructor(public navCtrl: NavController, 
    public navParams: NavParams,
    public modalCtrl: ModalController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ClientsAdministratorPage');
  }

  addCliente() {
    let addModal = this.modalCtrl.create('SignupPage', {pedirPassword: false} );
    addModal.onDidDismiss(category => {
   
    })
    addModal.present();
  
  }
}
