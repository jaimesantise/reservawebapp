import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ClientsAdministratorPage } from './clients-administrator';

@NgModule({
  declarations: [
    ClientsAdministratorPage,
  ],
  imports: [
    IonicPageModule.forChild(ClientsAdministratorPage),
  ],
})
export class ClientsAdministratorPageModule {}
